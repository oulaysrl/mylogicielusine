import unittest
from . import Account, Client

class TestAccount(unittest.TestCase):
    
    def setUp(self):
        self.client = Client(1, "2021/01/05", 1, "Bob", "Wilson")
        self.account = Account(555555, self.client, 500.0)
    
    def testOverdraft(self):
        self.account.withdraw(600)
        self.assertTrue(self.account.overdraft)
    
if __name__ == "__main__":
    unittest.main()
