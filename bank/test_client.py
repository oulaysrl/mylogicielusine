import unittest
from . import Client

class TestClient(unittest.TestCase):
    
    def setUp(self):
        self.client = Client(1, "2021/01/05", 1, "Bob", "Wilson")
    
    def testFullName(self):
        self.assertEqual("Bob WILSON", self.client.getFullName())
    
if __name__ == "__main__":
    unittest.main()