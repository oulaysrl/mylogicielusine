""" suite de tests unitaires du package bank """
import sys
import unittest
# import HtmlTestRunner
import xmlrunner
from bank import TestClient, TestAccount


def run_tests():
    """ lancement des tests sur client et account """
    suite = unittest.TestSuite()
    try:
        suite.addTest(unittest.TestLoader().loadTestsFromTestCase(TestClient))
        suite.addTest(unittest.TestLoader().loadTestsFromTestCase(TestAccount))
    except ValueError:
        print("unknown test, use: --help")
        sys.exit()

    return suite


if __name__ == "__main__":
    # runner = unittest.TextTestRunner()
    # runner = HtmlTestRunner.HTMLTestRunner(output="tests_artifacts")
    runner = xmlrunner.XMLTestRunner(output="tests_artifacts")
    runner.run(run_tests())
